package wsn.com.traceobject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;

public class LocalizationService extends Service {
    private LocationManager locationManager;
    private static final int LOCATION_INTERVAL = 5000;  // [ms]
    private static final float LOCATION_DISTANCE = 10f; // [m]

    public LocalizationService() {
    }



    private class GPSListener implements LocationListener {
        private Location lastLocation;

        public GPSListener(String provider) {
            Log.w("GPSListener", "Costruttore " + provider);
            lastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.w("GPSListener", "onLocationChanged " + location);
            lastLocation.set(location);
            ArrayList<ObjDescriptor> items = ObjDescriptorHelper.getItemList();
            for (ObjDescriptor o : items) {
                if (o.isGPSEnabled() && o.isTracked()) {
                    ObjDescriptorHelper.get(o).setGPSCoordinates(location);
                    Log.w("Location", "Posizione = " + location.getLatitude() + " " + location.getLongitude());
                }
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    private GPSListener listener;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.w("Localization", "Service creato");
        if (locationManager == null) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
        if (listener == null) {
            listener = new GPSListener(LocationManager.GPS_PROVIDER);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w("LocalizationService", "Interrompo il thread");
        if (locationManager != null) {
            locationManager.removeUpdates(listener);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, listener);
        return START_STICKY;
    }
}

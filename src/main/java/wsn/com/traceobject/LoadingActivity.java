package wsn.com.traceobject;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


public class LoadingActivity extends ActionBarActivity {

    private static final int REQUEST_CODE_ENABLE_BLUETOOTH = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        getWindow().getDecorView().setBackgroundColor(Color.LTGRAY);

        Thread tr = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch(InterruptedException ex) {
                    Log.getStackTraceString(ex);
                }
                if(!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                    final Intent in = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(in, REQUEST_CODE_ENABLE_BLUETOOTH);
                } else {
                    startApplication();
                }

            }
        });
        tr.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_loading, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_ENABLE_BLUETOOTH) {
            if(resultCode == Activity.RESULT_OK) {
                startApplication();
            } else {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
                mBuilder.setMessage(R.string.bluetoothAlertBox)
                        .setCancelable(true)
                        .setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setTitle("Notice");
                mBuilder.create().show();
            }
        }
    }

    private void startApplication() {
        ObjDescriptorHelper.getItemList(); //creating list
        Log.i("Loading", "Lancio activity main");
        Intent intent = new Intent(LoadingActivity.this, TraceObjectMainActivity.class);
        startActivity(intent);
        LoadingActivity.this.finish();
    }
}

package wsn.com.traceobject;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.kontakt.sdk.android.connection.OnServiceBoundListener;
import com.kontakt.sdk.android.manager.BeaconManager;

public class RangingService extends Service {
    private Thread thread;
    private BeaconManager beaconManager;

    public RangingService() {
    }

    private class ListenerThread extends Thread {
        @Override
        public void run() {
            Log.i("ListenerThread", "Sono partito");
            if(beaconManager.isConnected()) {
                try {
                    beaconManager.startRanging();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

            } else {
                connect();
            }
        }

        private void connect() {
            try {
                beaconManager.connect(new OnServiceBoundListener() {
                    @Override
                    public void onServiceBound() throws RemoteException {
                        beaconManager.startRanging();
                    }
                });
            } catch (RemoteException e) {

            }
        }
    }


    @Override
    public void onCreate() {
        beaconManager = BeaconManager.newInstance(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("RangingService", "Service partito");
        if (thread == null || !thread.isAlive()) {
            thread = new ListenerThread();
            thread.start();
        }
        if (thread.isInterrupted()) {
            thread.start();
        }
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        Log.i("Service", "Interrompo il thread");
        thread.interrupt();
        beaconManager.disconnect();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}

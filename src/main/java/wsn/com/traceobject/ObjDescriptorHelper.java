package wsn.com.traceobject;

import java.util.ArrayList;

/**
 * Created by Win on 18/07/2015.
 */
public class ObjDescriptorHelper {
    private static ArrayList<ObjDescriptor> itemList;
    public static synchronized ArrayList<ObjDescriptor> getItemList() {
        if (itemList == null) {
            itemList = new ArrayList<>();
        }
        return itemList;
    }

    public static synchronized void addItem(ObjDescriptor obj) {
        if (!itemList.contains(obj)) {
            itemList.add(obj);
        }
    }

    public static synchronized ObjDescriptor get(ObjDescriptor obj) {
        int index = itemList.indexOf(obj);
        return index < 0 ? null : itemList.get(index);
    }

    public static synchronized ObjDescriptor get(String id) {
        for (ObjDescriptor obj : itemList) {
            if (obj.getBeaconId().equals(id)) {
                return get(obj);
            }
        }
        return null;
    }

    public static synchronized void remove(ObjDescriptor obj) {
        itemList.remove(obj);
    }
}

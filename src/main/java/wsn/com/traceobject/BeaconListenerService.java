package wsn.com.traceobject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.kontakt.sdk.android.configuration.ForceScanConfiguration;
import com.kontakt.sdk.android.configuration.MonitorPeriod;
import com.kontakt.sdk.android.connection.OnServiceBoundListener;
import com.kontakt.sdk.android.device.BeaconDevice;
import com.kontakt.sdk.android.device.Region;
import com.kontakt.sdk.android.factory.AdvertisingPackage;
import com.kontakt.sdk.android.factory.Filters;
import com.kontakt.sdk.android.manager.BeaconManager;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BeaconListenerService extends Service implements BeaconManager.MonitoringListener {

    private boolean reset = true;

    private static final double ACCEPT_DISTANCE = 1.5;//[m]

    private static final int DEFAULT_PERIOD = 5;    // [s]

    private boolean receivedSomeSignal = false; // Check if I don't receive any signal => lost something

    private final BroadcastReceiver swipeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ObjDescriptor obj = intent.getParcelableExtra(getString(R.string.objDescriptor));
            if (ObjDescriptorHelper.getItemList().contains(obj)) {
                ObjDescriptorHelper.get(obj).found();
            }
            Log.w("BroadcastReceiver", "swipe");
            getApplicationContext().unregisterReceiver(this);
        }
    };

    private final BroadcastReceiver shutDownReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.w("kill service", "kill service");
            reset = false;
            stopSelf();
        }
    };

    private Thread listenerThread;

    private BeaconManager beaconManager;

    public BeaconListenerService() {
    }

    @Override
    public void onMonitorStart() {
        Log.w("Monitor", "Inizio monitoraggio");
        setSignalFlag(false);
    }

    @Override
    public void onMonitorStop() {
        Log.w("Monitor", "Fine monitoraggio");
        if (!receivedSomeSignal) {
            // onBeaconsUpdated never called. I have no beacons in my range. Notify I lost them all
            ArrayList<ObjDescriptor> monitoredItems = ObjDescriptorHelper.getItemList();
            for (ObjDescriptor obj : monitoredItems) {
                if (ObjDescriptorHelper.get(obj).isTracked()) {
                    while (!ObjDescriptorHelper.get(obj).incrementLostCounter()) ;
                    Log.e("OnMonitorStop", "perso = " + ObjDescriptorHelper.get(obj).isLost());
                    if (!ObjDescriptorHelper.get(obj).isNotified()) {
                        sendUINotification(true, obj);
                        ObjDescriptorHelper.get(obj).setNotified();
                    }
                    MyPrefs.updateItem(getApplicationContext(), ObjDescriptorHelper.get(obj));
                }
            }
        }

        new AsyncTask<Void, String, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Log.w("Thread", "Dormo");
                    Thread.sleep(TimeUnit.SECONDS.toMillis(DEFAULT_PERIOD));
                    Log.w("Thread", "Sveglio");
                } catch (InterruptedException e) {
                    Log.e("Asynctask receiver", Log.getStackTraceString(e));
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                stopSelf();
            }
        }.execute();
    }

    @Override
    public void onBeaconsUpdated(Region region, List<BeaconDevice> beaconDevices) {
        /*You can send broadcast with entire list of Beacon devices.
        * However, please be aware of Bundle limitations.*/
        Log.w("Monitor", "onBeaconsUpdated");
        String msg = "";
        for (BeaconDevice b : beaconDevices) {
            msg = msg.concat(b.getUniqueId() + ", ");
        }
        Log.w("BeaconListenerService", msg.substring(0, msg.lastIndexOf(",")));
        ArrayList<ObjDescriptor> monitoredItems = ObjDescriptorHelper.getItemList();
        boolean contained;
        for (ObjDescriptor obj : monitoredItems) {
            contained = false;
            if (!obj.isTracked()) {
                continue;
            }
            for (BeaconDevice bd : beaconDevices) {
                if (obj.equals(bd)) {
                    contained = true;
                    if (ObjDescriptorHelper.get(obj).isLost()) {
                        Log.w("OGGETTO RITROVATO", "Hai ritrovato " + obj.getName());
                        sendUINotification(false, obj);
                        reset = true;
                    }
                    ObjDescriptorHelper.get(obj).found();
                    break;
                }
            }
            if (!contained) {
                // I may have lost obj
                if (ObjDescriptorHelper.get(obj).incrementLostCounter() && !ObjDescriptorHelper.get(obj).isNotified()) {
                    Log.w("OGGETTO PERSO", "Hai perso " + obj.getName());
                    sendUINotification(true, obj);
                    ObjDescriptorHelper.get(obj).setNotified();
                }
            }
            MyPrefs.updateItem(getApplicationContext(), ObjDescriptorHelper.get(obj));
        }
        setSignalFlag(true);

    }


    @Override
    public void onBeaconAppeared(Region region, BeaconDevice beaconDevice) {
        Log.w("Monitor", "onBeaconAppeared");
    }

    @Override
    public void onRegionEntered(Region region) {
        Log.w("Monitor", "onRegionEntered");
    }

    @Override
    public void onRegionAbandoned(Region region) {
        Log.w("Monitor", "onRegionAbandoned");
    }

    private void sendUINotification(boolean lost, ObjDescriptor obj) {
        NotificationCompat.Builder mBuild = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM))
                .setContentTitle("Trace Object");

        // If the main activity is opened, then clicking on the notification must not open any activity
        Intent intent = new Intent(this, TraceObjectMainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent resIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        mBuild.setContentIntent(resIntent);

        // Set the deleteIntent to catch the "swipe" action on the notification
        Intent j = new Intent(getString(R.string.deleteIntent));
        j.putExtra(getString(R.string.objDescriptor), ObjDescriptorHelper.get(obj));
        PendingIntent delIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, j, PendingIntent.FLAG_UPDATE_CURRENT);
        getApplicationContext().registerReceiver(swipeReceiver, new IntentFilter(getString(R.string.deleteIntent)));
        mBuild.setDeleteIntent(delIntent);

        // Send and intent to color the ListView in main activity

        Intent colorIntent = new Intent(getString(R.string.colorIntent));
        colorIntent.putExtra(getString(R.string.objDescriptor), obj);
        colorIntent.putExtra(getString(R.string.isLost), lost);
        LocalBroadcastManager.getInstance(this).sendBroadcast(colorIntent);


        int id = obj.getBeaconId().hashCode();
        if (lost) {
            mBuild.setContentText("You lost " + obj.getName());
            if (obj.areCoordinatesSet()) {
                // Set the "view map" button on the notification
                double latitude = obj.getCoordinates()[0];
                double longitude = obj.getCoordinates()[1];
                String point = latitude + "," + longitude;
                String uriString = getString(R.string.maps_uri) + point + " " + obj.getName();
                Uri uri = Uri.parse(uriString);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
                mapIntent.setPackage("com.google.android.apps.maps");
                PendingIntent mapPending = PendingIntent.getActivity(getApplicationContext(), 0, mapIntent, 0);
                mBuild.addAction(R.drawable.maps, "View map", mapPending);
            }
        } else {
            mBuild.setContentTitle("Trace Object").setContentText("You found " + obj.getName());
        }
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.notify(id, mBuild.build());
    }

    private synchronized boolean getSignalFlag() {
        return receivedSomeSignal;
    }

    private synchronized void setSignalFlag(boolean f) {
        receivedSomeSignal = f;
    }

    class ListenerThread extends Thread {
        @Override
        public void run() {
            Log.w("BeaconListenerService", "Thread partito");
            if (beaconManager.isConnected()) {
                try {
                    beaconManager.startMonitoring();
                } catch (RemoteException e) {
                    Log.getStackTraceString(e);
                }
            } else {
                connect();
            }
            while (!isInterrupted()) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    Log.w("Thread", "Ho ricevuto interrupt, sono nel catch");
                    beaconManager.stopMonitoring();
                    beaconManager.disconnect();
                } finally {
                    return;
                }
            }

        }

        private void connect() {
            try {
                beaconManager.connect(new OnServiceBoundListener() {
                    @Override
                    public void onServiceBound() throws RemoteException {
                        beaconManager.startMonitoring();
                    }
                });
            } catch (RemoteException e) {
                Log.getStackTraceString(e);
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("BeaconListenerService", "onCreate");

        beaconManager = BeaconManager.newInstance(this);
//        beaconManager.setScanMode(BeaconManager.SCAN_MODE_BALANCED); utile solo a lollipop
//        beaconManager.setMonitorPeriod(MonitorPeriod.MINIMAL);
//        beaconManager.setBeaconActivityCheckConfiguration(BeaconActivityCheckConfiguration.DEFAULT);
        beaconManager.setForceScanConfiguration(ForceScanConfiguration.DEFAULT);
        beaconManager.registerMonitoringListener(this);

        beaconManager.addFilter(new Filters.CustomFilter() {
            @Override
            public Boolean apply(AdvertisingPackage object) {
                final UUID proximityUUID = object.getProximityUUID();
                final double distance = object.getAccuracy();

                return proximityUUID.equals(BeaconManager.DEFAULT_KONTAKT_BEACON_PROXIMITY_UUID) && distance <= ACCEPT_DISTANCE;
//                Log.w("FILTRO", object.getBeaconUniqueId() + "  " + object.getAccuracy() + " " + object.getProximity());

//                return (distance <= ACCEPT_DISTANCE) && (object.getProximity() != Proximity.FAR);

            }
        });

        // Register shutdown receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(shutDownReceiver, new IntentFilter(getString(R.string.kill_listener_service)));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("Service", "Service creato, creo listenerThread");

        // Set the monitoring period in milliseconds. Active period = 5s, passive may be passed via Intent. Default = 5s
        beaconManager.setMonitorPeriod(new MonitorPeriod(TimeUnit.SECONDS.toMillis(DEFAULT_PERIOD), TimeUnit.SECONDS.toMillis(intent.getIntExtra("Period", DEFAULT_PERIOD))));
        if (listenerThread == null || !listenerThread.isAlive()) {
            listenerThread = new ListenerThread();
            Log.i("Service", "Ho creato il listenerThread");
            listenerThread.start();
        }
        // forse il controllo isInterrupted non è necessario
        if (listenerThread.isInterrupted()) {
            listenerThread.start();
            Log.i("Service", "Thread era interrotto. Ripartito");
        }

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            Log.i("BeaconListenerService", "Interrompo il listenerThread");
            listenerThread.interrupt();

            beaconManager.stopMonitoring();
            if (beaconManager.isConnected()) {
                beaconManager.disconnect();
            }
        } catch (NullPointerException ex) {
            Log.e("BeaconListenerService", "Catch onDestroy. provo a resettare applicazione");
            Intent intent = new Intent(getApplicationContext(), LoadingActivity.class);
            startActivity(intent);

        }
        if (reset) {
            Intent resetIntent = new Intent(getString(R.string.reset_listener_service));
            LocalBroadcastManager.getInstance(this).sendBroadcast(resetIntent);
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}

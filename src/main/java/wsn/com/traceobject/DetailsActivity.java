package wsn.com.traceobject;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsActivity extends ActionBarActivity {
    private Switch gpsSwitch;
    private Switch trackSwitch;
    private EditText newName;
    private Button delete;
    private Button showMap;
    private ObjDescriptor descriptor;

    @Override
    public void onBackPressed() {
        if (descriptor == null || descriptor.getName().equals(newName.getText().toString())) {
            updateDescriptor();
            Intent i = new Intent(this, TraceObjectMainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        } else {
            handleNewName(true);
        }
    }

    private void handleNewName(final boolean isBackButtonPressed) {
        final String name = newName.getText().toString();
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetailsActivity.this);
        mBuilder.setMessage("Are you sure you want to change the name?")
                .setCancelable(true)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (name.isEmpty()) {
                            Toast.makeText(DetailsActivity.this, "The name can not be empty", Toast.LENGTH_SHORT).show();
                            newName.setText(descriptor.getName());
                            newName.setSelection(newName.getText().length());
                        } else {
                            descriptor.setNewName(name);
                            newName.setSelection(newName.getText().length());
                            Toast.makeText(DetailsActivity.this, "Name changed", Toast.LENGTH_SHORT).show();
                            if (isBackButtonPressed) {
                                DetailsActivity.this.finish();
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        newName.setText(descriptor.getName());
                        newName.setSelection(newName.getText().length());
                        Toast.makeText(DetailsActivity.this, "Name not changed", Toast.LENGTH_SHORT).show();
                    }
                })
                .setTitle("Notice");
        mBuilder.create().show();
    }

    private void updateDescriptor() {
        if (descriptor == null) {
            //TODO Cancellare poi quando sistemo tasto delete
            return;
        }
        ObjDescriptorHelper.remove(descriptor);
        ObjDescriptorHelper.addItem(descriptor);
        Log.w("updateDescriptor", "descriptor.isLost() " + descriptor.isLost());
        MyPrefs.updateItem(this, descriptor);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Intent in = getIntent();
        descriptor = (ObjDescriptor) in.getExtras().get(getString(R.string.objDescriptor));
        newName = (EditText) findViewById(R.id.newName);
        setTitle(descriptor.getName());
        gpsSwitch = (Switch) findViewById(R.id.gpsEnabler);
        gpsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    descriptor.enableGPS();
                } else {
                    descriptor.disableGPS();
                }
            }
        });

        trackSwitch = (Switch) findViewById(R.id.enableSwitch);
        trackSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    descriptor.enableTracking();
                } else {
                    descriptor.disableTracking();
                }
            }
        });


        newName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE ||
                        event.getAction() == KeyEvent.ACTION_DOWN &&
                                (event.getKeyCode() == KeyEvent.KEYCODE_ENTER ||
                                        event.getKeyCode() == KeyEvent.KEYCODE_BACK)) {
                    handleNewName(false);

                }
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });

        //DEBUG
        delete = (Button) findViewById(R.id.deleteBtn);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetailsActivity.this);
                mBuilder.setMessage("Are you sure you want to delete " + descriptor.getName() + "?")
                        .setCancelable(true)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MyPrefs.deleteItem(getApplicationContext(), descriptor);
                                ObjDescriptorHelper.remove(descriptor);
                                descriptor = null;
                                finish();
                            }
                        }).setTitle("Confirm");
                mBuilder.create().show();
            }
        });

        showMap = (Button) findViewById(R.id.getLastGPS);
        showMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!descriptor.areCoordinatesSet()) {
                    Toast.makeText(DetailsActivity.this, descriptor.getName() + " has no saved position", Toast.LENGTH_SHORT).show();
                } else {
                    double latitude = descriptor.getCoordinates()[0];
                    double longitude = descriptor.getCoordinates()[1];
                    String point = latitude + "," + longitude;
                    String uriString = getString(R.string.maps_uri) + point + " " + descriptor.getName();
                    Uri uri = Uri.parse(uriString);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            }
        });
//        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        descriptor = ObjDescriptorHelper.get(descriptor);
        gpsSwitch.setChecked(descriptor.isGPSEnabled());
        trackSwitch.setChecked(descriptor.isTracked());
        newName.setText(descriptor.getName());
        newName.setSelection(newName.getText().length());
    }

    @Override
    protected void onPause() {
        super.onPause();
        updateDescriptor();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            if (descriptor == null || descriptor.getName().equals(newName.getText().toString())) {
                updateDescriptor();
            } else {
                handleNewName(true);
            }
        }
        return super.onOptionsItemSelected(item);
    }
}

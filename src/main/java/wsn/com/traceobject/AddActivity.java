package wsn.com.traceobject;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.kontakt.sdk.android.configuration.BeaconActivityCheckConfiguration;
import com.kontakt.sdk.android.configuration.ForceScanConfiguration;
import com.kontakt.sdk.android.connection.OnServiceBoundListener;
import com.kontakt.sdk.android.data.RssiCalculators;
import com.kontakt.sdk.android.device.BeaconDevice;
import com.kontakt.sdk.android.device.Region;
import com.kontakt.sdk.android.manager.BeaconManager;
import java.util.List;

public class AddActivity extends ActionBarActivity {

    private ListView list;
    private BeaconBaseAdapter adapter;
    private ProgressBar progressBar;
    private TextView textView;
    private Button ok;
    private EditText name;

    private int selectedPosition = -1;
    private boolean refreshButtonEnabled = true;

    private BeaconManager beaconManager;
    private static final int REQUEST_CODE_ENABLE_BLUETOOTH = 1;

    private class BackgroundScan extends AsyncTask<Void, String, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("add", "asynctask partito");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Log.getStackTraceString(e);
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (beaconManager == null) {
                return;
            }
            if(beaconManager.isConnected()) {
               Log.i("AsyncTask", "StopRanging()");
                beaconManager.stopRanging();
            }
            progressBar.setVisibility(ProgressBar.GONE);
            textView.setText("Done");
            list.setEnabled(true);
            refreshButtonEnabled = true;
            invalidateOptionsMenu();
        }

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        name = (EditText) findViewById(R.id.newName);
        list = (ListView) findViewById(R.id.listViewNewObj);
        adapter = new BeaconBaseAdapter(this);
        list.setAdapter(adapter);
        progressBar = (ProgressBar) findViewById(R.id.scanningProgressBar);
        textView = (TextView) findViewById(R.id.scanningTextView);
        ok = (Button) findViewById(R.id.addConfirm);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("onclick", Integer.toString(selectedPosition));
                if (selectedPosition == -1 || name.getText().toString().isEmpty()) {
                    Toast.makeText(AddActivity.this, "You must select a device and a name first", Toast.LENGTH_SHORT).show();
                    return;
                }

                // Save in Shared Preferences the new object
                BeaconDevice beacon = (BeaconDevice) adapter.getItem(selectedPosition);
                ObjDescriptor descriptor = new ObjDescriptor(name.getText().toString(), beacon.getUniqueId(),
                        ((Switch) findViewById(R.id.gpsByDefault)).isChecked(), ((Switch) findViewById(R.id.newItemEnableSwitch)).isChecked());
                String beaconAttrs = descriptor.toString();

                if (MyPrefs.saveNewItem(AddActivity.this, beacon.getUniqueId(), beaconAttrs)) {
                    // Save in the shared itemList
                    ObjDescriptorHelper.addItem(descriptor);
                    finish();
                } else {
                    // Selected beacon already saved
                    Log.w("Cant save", "Beacon già utilizzato");
                    Toast.makeText(AddActivity.this, "The selected beacon is already associated with an object. Select another beacon or disassociate it", Toast.LENGTH_LONG).show();
                }
            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            private final int DEFAULT_COLOR = Color.parseColor("#eeeeee");
            private final int SELECTED_COLOR = Color.parseColor("#aaaaaa");

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                view.setActivated(true);

                // Sets all backgrounds to default value

                for (int i = 0; i < list.getChildCount(); i++) {
                    list.getChildAt(i).setBackgroundColor(DEFAULT_COLOR);
                }
                view.setBackgroundColor(SELECTED_COLOR);

                selectedPosition = position;
            }
        });

        beaconManager = BeaconManager.newInstance(this);
        beaconManager.setRssiCalculator(RssiCalculators.newLimitedMeanRssiCalculator(5)); //Calculate rssi value basing on arithmethic mean basing on 5 last notified values
        beaconManager.setBeaconActivityCheckConfiguration(BeaconActivityCheckConfiguration.DEFAULT);

        beaconManager.setForceScanConfiguration(ForceScanConfiguration.DEFAULT);

        beaconManager.registerRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(final Region region, final List<BeaconDevice> beacons) {
                AddActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.replaceWith(beacons);
                    }
                });
            }
        });
    }

    private void scanDevices() {
        refreshButtonEnabled = false;
        invalidateOptionsMenu();
        textView.setText("Scanning devices");
        progressBar.setVisibility(ProgressBar.VISIBLE);
        list.setEnabled(false);

        new BackgroundScan().execute();
        if(! beaconManager.isBluetoothEnabled()){
            final Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_CODE_ENABLE_BLUETOOTH);
        } else if(beaconManager.isConnected()) {
            startRanging();
        } else {
            connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        scanDevices();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        int icon = refreshButtonEnabled ? R.drawable.ic_autorenew_white_24dp : R.drawable.ic_autorenew_black_24dp;
        menu.findItem(R.id.action_refresh).setEnabled(refreshButtonEnabled).setIcon(icon);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            scanDevices();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startRanging() {
        Log.i("ranging", "StartRanging()");
        try {
            beaconManager.startRanging();
        } catch (RemoteException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void connect() {
        Log.i("connect", "connect()");

        try {
            beaconManager.connect(new OnServiceBoundListener() {
                @Override
                public void onServiceBound() throws RemoteException {
                    beaconManager.startRanging();
                }
            });
        } catch (RemoteException e) {
            Toast.makeText(AddActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_ENABLE_BLUETOOTH) {
            if(resultCode != Activity.RESULT_OK) {
                Toast.makeText(AddActivity.this, "Bluetooth is not enabled", Toast.LENGTH_LONG).show();
            }
            new BackgroundScan().execute();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}

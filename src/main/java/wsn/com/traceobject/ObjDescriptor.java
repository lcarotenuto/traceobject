package wsn.com.traceobject;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.kontakt.sdk.android.device.BeaconDevice;


/**
 * Created by Win on 13/07/2015.
 */
public class ObjDescriptor implements Parcelable {
    private String name;
    private String uniqueId;
    private boolean gpsEnabled;
    private boolean coordinatesSet;
    private boolean lost;
    private boolean notified;
    private boolean trackingEnabled;
    private double latitude;
    private double longitude;
    private int lostCounter;

    public ObjDescriptor(String n, String id, boolean gps, boolean tracked) {
        name = n;
        uniqueId = id;
        gpsEnabled = gps;
        coordinatesSet = false;
        trackingEnabled = tracked;
        lost = false;
        lostCounter = 0;
    }


    public void setGPSCoordinates(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        coordinatesSet = true;
    }

    public boolean areCoordinatesSet() {
        return coordinatesSet;
    }

    public void disableGPS() {
        gpsEnabled = false;
    }

    public void enableGPS() {
        gpsEnabled = true;
    }

    public boolean isGPSEnabled() {
        return gpsEnabled;
    }

    public String getName() {
        return name;
    }

    public String getBeaconId() {
        return uniqueId;
    }

    public void found() {
        lost = false;
        lostCounter = 0;
        notified = false;
    }

    public boolean isLost() {
        return lost;
    }

    public void enableTracking() {
        trackingEnabled = true;
    }

    public void disableTracking() {
        trackingEnabled = false;
    }

    public boolean isTracked() {
        return trackingEnabled;
    }

    public double[] getCoordinates() {
        return new double[]{latitude, longitude};
    }

    public boolean incrementLostCounter() {
        if (!lost) {
            lostCounter++;
            if (lostCounter == 3) {
                lost = true;
            }
        }
        return lost;
    }

    public void setNotified() {
        notified = true;
    }


    public boolean isNotified() {
        return notified;
    }


    @Override
    public String toString() {
        String ret = name + ";" + uniqueId + ";" + gpsEnabled + ";" + coordinatesSet + ";" + lost + ";" +
            notified + ";" + trackingEnabled + ";" + latitude + ";" + longitude + ";" + lostCounter;
        return ret;
    }

    public ObjDescriptor (String s) {
        String[] splitted = s.split(";");
        name = splitted[0];
        uniqueId = splitted[1];
        gpsEnabled = Boolean.parseBoolean(splitted[2]);
        coordinatesSet = Boolean.parseBoolean(splitted[3]);
        lost = Boolean.parseBoolean(splitted[4]);
        notified = Boolean.parseBoolean(splitted[5]);
        trackingEnabled = Boolean.parseBoolean(splitted[6]);
        latitude = Double.parseDouble(splitted[7]);
        longitude = Double.parseDouble(splitted[8]);
        lostCounter = Integer.parseInt(splitted[9]);
    }



    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ObjDescriptor) {
            return ((ObjDescriptor) obj).getBeaconId().equals(uniqueId);
        }
        if (obj instanceof BeaconDevice) {
            return ((BeaconDevice) obj).getUniqueId().equals(uniqueId);
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(uniqueId);
        dest.writeByte((byte) (gpsEnabled ? 1 : 0));
        dest.writeByte((byte) (coordinatesSet ? 1 : 0));
        dest.writeByte((byte) (lost ? 1 : 0));
        dest.writeByte((byte) (notified ? 1 : 0));
        dest.writeByte((byte) (trackingEnabled ? 1 : 0));
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(lostCounter);

    }

    public static final Parcelable.Creator<ObjDescriptor> CREATOR = new Parcelable.Creator<ObjDescriptor>() {

        @Override
        public ObjDescriptor createFromParcel(Parcel source) {
            return new ObjDescriptor(source);
        }

        @Override
        public ObjDescriptor[] newArray(int size) {
            return new ObjDescriptor[size];
        }
    };

    public ObjDescriptor(Parcel in) {
        name = in.readString();
        uniqueId = in.readString();
        gpsEnabled = in.readByte() != 0;    // If 1 => true, false otherwise
        coordinatesSet = in.readByte() != 0;
        lost = in.readByte() != 0;
        notified = in.readByte() != 0;
        trackingEnabled = in.readByte() != 0;
        latitude = in.readDouble();
        longitude = in.readDouble();
        lostCounter = in.readInt();

    }

    public void setNewName(String newName) {
        name = newName;
    }
}
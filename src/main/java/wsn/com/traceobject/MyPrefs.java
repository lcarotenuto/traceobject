package wsn.com.traceobject;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Win on 18/08/2015.
 */
public class MyPrefs {
    private static final String PREF_NAME = "SAVED_BEACONS";    // Name of file of SharedPrefs
    private static final String ID_LIST = "ID_LIST";            // To retrieve the list of IDs
    private static final String SERVICE_STATUS = "SERVICE_STATUS";  // Get service status for TraceObjectMainActivity
    private static final String GPSStatus = "GPS_STATUS";       // Get gps service status for TraceObjectMainActivity


    public static LinkedHashSet<String> getSavedIDs(Context c) {
        SharedPreferences prefs = c.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Set<String> retrieved = prefs.getStringSet(ID_LIST, new LinkedHashSet<String>());
        return new LinkedHashSet<String>(retrieved);
    }

    public static String getAttributes(Context c, String id) {
        return getSelectedPrefs(c, id);
    }

    public static String getServiceStatus(Context c) {
        return getSelectedPrefs(c, SERVICE_STATUS);
    }

    public static String getGPSStatus(Context c) {
        return getSelectedPrefs(c, GPSStatus);
    }

    private static String getSelectedPrefs(Context c, String key) {
        SharedPreferences prefs = c.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, "");
    }

    /**
     * This method saves the new item using SharedPreferences
     * @param c Context to pass
     * @param id Id of new item
     * @param obj Attributes
     * @return true if all good, false otherwise
     */

    public static boolean saveNewItem(Context c, String id, String obj) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        // Save new ID
        LinkedHashSet<String> newIDs = getSavedIDs(c);
        // If I try to add an object already saved (i.e. already used) return false
        if (!newIDs.add(id)) {
            return false;
        }
        editor.putStringSet(ID_LIST, newIDs).putString(id, obj);
        editor.apply();
        return true;
    }

    public static void saveServiceStatus(Context c, boolean status) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(SERVICE_STATUS, Boolean.toString(status));
        editor.apply();
    }

    public static void saveGPSStatus(Context c, boolean status) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(GPSStatus, Boolean.toString(status));
        editor.apply();
    }

    public static void deleteItem(Context c, String id) {
        SharedPreferences sharedPrefs = c.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        LinkedHashSet<String> ids = getSavedIDs(c);
        boolean ret = ids.remove(id);
        editor.remove(ID_LIST);
        editor.putStringSet(ID_LIST, ids);
        editor.remove(id);
        editor.apply();
    }

    public static void deleteItem(Context c, ObjDescriptor obj) {
        deleteItem(c, obj.getBeaconId());
    }

    /**
     * ONLY FOR DEBUG PURPOSES
     * Delete all shared preferences
     * @param c Context
     */
    public static void deleteSharedPreferences(Context c) {
        SharedPreferences prefs = c.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        prefs.edit().clear().commit();
        Log.w("SharedPrefs", "Shared Preferences eliminate");
    }

    public static void updateItem(Context c, ObjDescriptor obj) {
        deleteItem(c, obj.getBeaconId());
        saveNewItem(c, obj.getBeaconId(), obj.toString());
    }
}

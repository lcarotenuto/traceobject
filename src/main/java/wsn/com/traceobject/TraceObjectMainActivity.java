package wsn.com.traceobject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import java.util.LinkedHashSet;
import java.util.List;


public class TraceObjectMainActivity extends ActionBarActivity {
    private ListView objList;
    private ItemAdapter myAdapter;
    private Switch bServiceSwitch;
    private Switch gpsServiceSwitch;


    class ItemAdapter extends BaseAdapter {

        private List<ObjDescriptor> itemList;

        public ItemAdapter(List<ObjDescriptor> items) {
            itemList = items;
        }

        @Override
        public int getCount() {
            itemList = ObjDescriptorHelper.getItemList();
            return itemList.size();
        }

        @Override
        public Object getItem(int position) {
            itemList = ObjDescriptorHelper.getItemList();
            return itemList.get(position);
        }

        @Override
        public long getItemId(int position) {
            itemList = ObjDescriptorHelper.getItemList();
            return itemList.get(position).hashCode();
        }

        public int getPosition(Object o) {
            return itemList.lastIndexOf(o);
        }

        public void clear() {
            itemList = ObjDescriptorHelper.getItemList();
            itemList.clear();
            super.notifyDataSetChanged();
        }

        public void add(ObjDescriptor obj) {
            itemList = ObjDescriptorHelper.getItemList();
            if (!itemList.contains(obj)) {
                itemList.add(obj);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.beacon_list_view_main, null);
                viewHolder = new ViewHolder();
                viewHolder.item = (TextView) convertView.findViewById(R.id.itemName);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            ObjDescriptor obj = (ObjDescriptor) getItem(position);
            viewHolder.item.setText("Name: " + obj.getName() + "; ID: " + obj.getBeaconId());
            viewHolder.item.setTextColor(Color.BLACK);
            if (obj.isLost()) {
                convertView.setBackgroundColor(Color.RED);
            }
            super.notifyDataSetChanged();
            return convertView;
        }

        private class ViewHolder {
            public TextView item;
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.w("new Broadcast receiver", "Ricevuto intent colorato");
            ObjDescriptor obj = (ObjDescriptor) intent.getExtras().get(getString(R.string.objDescriptor));
            boolean lost = intent.getBooleanExtra(getString(R.string.isLost), true);
            int i;
            for (i = 0; i < objList.getChildCount(); i++) {
                if (obj.equals(myAdapter.getItem(i))) {
                    break;
                }
            }
            int color = lost ? Color.RED : Color.parseColor("#eeeeee");
            objList.getChildAt(i).setBackgroundColor(color);
        }
    };

    private BroadcastReceiver resetListenerServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.w("RESET LISTENER", "RESETTO IL LISTENER");
            Intent i = new Intent(TraceObjectMainActivity.this, BeaconListenerService.class);
            startService(i);
        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trace_object_main);
        objList = (ListView) findViewById(R.id.ObjectList);
        myAdapter = new ItemAdapter(ObjDescriptorHelper.getItemList());
        objList.setAdapter(myAdapter);
        bServiceSwitch = (Switch) findViewById(R.id.enable_all_switch);
        bServiceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Intent i = new Intent(TraceObjectMainActivity.this, BeaconListenerService.class);
                if (isChecked) {
                    startService(i);
                } else {
                    Intent intent = new Intent(getString(R.string.kill_listener_service));
                    LocalBroadcastManager.getInstance(TraceObjectMainActivity.this).sendBroadcast(intent);
                }
                MyPrefs.saveServiceStatus(TraceObjectMainActivity.this, bServiceSwitch.isChecked());
            }

        });
        gpsServiceSwitch = (Switch) findViewById(R.id.gps_all_enabled);
        gpsServiceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Intent i = new Intent(TraceObjectMainActivity.this, LocalizationService.class);
                if (isChecked) {
                    if (!((LocationManager) getSystemService(Context.LOCATION_SERVICE))
                            .isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        final Intent in = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(in);
                        gpsServiceSwitch.setChecked(false);
                    } else {
                        startService(i);
                    }
                } else {
                    stopService(i);
                }
                MyPrefs.saveGPSStatus(TraceObjectMainActivity.this, isChecked);
            }
        });
        objList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ObjDescriptor obj = (ObjDescriptor) parent.getItemAtPosition(position);
                Log.w("selezionato", obj.toString());

                Intent intent = new Intent(TraceObjectMainActivity.this, DetailsActivity.class);
                intent.putExtra(getString(R.string.objDescriptor), obj);
                startActivity(intent);
            }
        });

        // Broadcast receiver to color lost items
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(getString(R.string.colorIntent)));

        // Broadcast receiver to reset the service
        LocalBroadcastManager.getInstance(this).registerReceiver(resetListenerServiceReceiver, new IntentFilter(getString(R.string.reset_listener_service)));

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.w("TraceObjectMainActivity", "onResume()");
        myAdapter.clear();

        // Set monitor on/off, depending on how was set
        String status = MyPrefs.getServiceStatus(this);
        bServiceSwitch.setChecked(Boolean.parseBoolean(status));

        // Set GPS on/off
        status = MyPrefs.getGPSStatus(this);
        gpsServiceSwitch.setChecked(Boolean.parseBoolean(status));


        // Load beacons from shared Prefs
        try {
            LinkedHashSet<String> idList = MyPrefs.getSavedIDs(this);
            for (String id : idList) {
                String attrs = MyPrefs.getAttributes(this, id);
                ObjDescriptor obj = new ObjDescriptor(attrs);

                ObjDescriptorHelper.addItem(obj);
                myAdapter.add(obj);
                myAdapter.notifyDataSetChanged();
            }
        } catch (NullPointerException ex) {
            Log.w("TraceObjectMainActivity", "NullPointerException. Reset applicazione");
            Log.getStackTraceString(ex);
            Intent intent = new Intent(this, LoadingActivity.class);
            startActivity(intent);
            finish();
        } catch (Exception ex) {
            MyPrefs.deleteSharedPreferences(this);
            Log.e("TraceObjectMainActivity", "onResume, catch dell'eccezione");
            Log.e("Catch eccezione", Log.getStackTraceString(ex));
            finish();
        }


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent i = new Intent(this, BeaconListenerService.class);
        stopService(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_trace_object_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add) {
            Intent i = new Intent(this, AddActivity.class);
            startActivity(i);
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
